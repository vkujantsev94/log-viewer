import tkinter as tk
from widgets import MainWidget
import settings


if __name__ == "__main__":
    settings.init()

    root = tk.Tk()
    root.resizable(height=True, width=True)
    mw = MainWidget(root)
    mw.grid(row=0, column=0, sticky='nswe')
    root.columnconfigure(0, weight=1)
    root.rowconfigure(1, weight=1)
    root.mainloop()
