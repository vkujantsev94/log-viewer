import tkinter as tk
from tkinter import messagebox as mb
import settings
from utils import form_message_selection, prepare_url_from_region, get_devlogs, clear_data_storage, get_max_word_length
from parse_logs import preprocess_raw_logs

colors = {
    "incoming": {
        "default": "DarkOliveGreen3",
        "active": "OliveDrab1",
        "selected": "chartreuse2"
    },
    "outcoming": {
        "default": "LightSalmon2",
        "active": "LightSalmon3",
        "selected": "tomato2"
    }
}


class RowWidget(tk.Frame):
    def __init__(self, master, names=None):
        tk.Frame.__init__(self, master)
        self.label_abs_time = tk.Label(self)
        self.label_rel_time = tk.Label(self)
        self.label_segment_id = tk.Label(self)
        self.label_type = tk.Label(self)
        self.label_text = tk.Label(self, wraplength=300)

        self.labels = [self.label_abs_time, self.label_rel_time, self.label_segment_id, self.label_type,
                       self.label_text]
        self.num_columns = len(self.labels)
        if names is not None:
            for l, n in zip(self.labels, names):
                # wrap_len = max(map(len, n.split(' ')))
                l.config(text='\n'.join(n.split(' ')))
        for i, l in enumerate(self.labels):
            l.grid(row=0, column=i, sticky='ns')
            l.config(relief="ridge")


class MsgRowWidget(RowWidget):
    def __init__(self, master, msg):
        RowWidget.__init__(self, master)
        self.msg = msg
        # self.segment = settings.DATA_STORAGE['segments'][msg.segment_id]
        self.label_abs_time.config(text=msg.absolute_time)
        if msg.type == 'PotentialPause' and msg.relative_time and abs(int(msg.relative_time)) > 250:
            color = 'red'
        else:
            color = 'black'
        self.label_rel_time.config(text=msg.relative_time, fg=color)
        self.label_segment_id.config(text=msg.segment_id)
        self.label_type.config(text=msg.type)
        text = msg.text if msg.text is not None else '--'
        self.label_text.config(text=text, wraplength=400)

        self.change_self_color_to_default()

        if self.msg.incoming:
            self.bind('<Leave>', self.change_segment_color_to_default)
            self.bind('<Enter>', self.change_segment_color_to_selected)
        else:
            self.bind('<Leave>', self.change_self_color_to_default)
            self.bind('<Enter>', self.change_self_color_to_selected)

    def change_self_color_to_default(self, event=None):
        for l in self.labels:
            if self.msg.incoming:
                l['bg'] = colors['incoming']['default']
            else:
                l['bg'] = colors['outcoming']['default']

    def change_self_color_to_active(self, event=None):
        for l in self.labels:
            if self.msg.incoming:
                l['bg'] = colors['incoming']['active']
            else:
                l['bg'] = colors['outcoming']['active']

    def change_self_color_to_selected(self, event=None):
        for l in self.labels:
            if self.msg.incoming:
                l['bg'] = colors['incoming']['selected']
            else:
                l['bg'] = colors['outcoming']['selected']

    def get_segment(self):
        data = settings.DATA_STORAGE
        segment = data['segments'][self.msg.segment_id]
        return segment

    def get_widgets_from_same_segment(self):
        segment = self.get_segment()
        segment_widgets = [settings.DATA_STORAGE['widgets'][msg.ind] for msg in segment.segment_msg_generator()]
        return segment_widgets

    def change_segment_color_to_selected(self, event=None):
        segment_widgets = self.get_widgets_from_same_segment()
        for w in segment_widgets:
            w.change_self_color_to_active()
        self.change_self_color_to_selected()
        reaction_label['text'] = self.get_segment().get_segment_reaction()

    def change_segment_color_to_default(self, event=None):
        segment_widgets = self.get_widgets_from_same_segment()
        for w in segment_widgets:
            w.change_self_color_to_default()
        reaction_label['text'] = default_reaction_label_text


class MsgBoardWidget(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.canvas = tk.Canvas(self, borderwidth=0, width=0, background="#ffffff")
        self.frame = tk.Frame(self.canvas, background="#ffffff")
        self.vsb = tk.Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.canvas.grid(row=0, column=0, sticky='nswe')
        self.vsb.grid(row=0, column=1, sticky='ns')
        self.window = self.canvas.create_window((4, 4), window=self.frame, anchor="nw",
                                                tags="self.frame")
        # self.window.resizable(height=True, width=True)
        self.frame.bind("<Configure>", self.on_frame_configure)

        self.rows = []
        self.header = RowWidget(self.frame, ['absolute time ms', 'relative time ms', 'segment id', 'message type', 'text'])
        self.header.grid(row=0, column=0, sticky='nw')
        self.align_columns()

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

    def on_frame_configure(self, event):
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def align_columns(self):
        if len(self.rows) == 0: return
        num_columns = self.rows[0].num_columns
        for col_id in range(num_columns - 1):
            max_col_width = 0
            for row in self.rows + [self.header]:
                cell_text = str(row.labels[col_id]['text'])
                max_col_width = max(get_max_word_length(cell_text), max_col_width)
            for row in self.rows + [self.header]:
                row.labels[col_id]['width'] = max_col_width
            # self.header.labels[col_id].config(wraplength=max_col_width)

    def clear(self):
        for row in self.rows:
            row.destroy()
        self.rows.clear()
        settings.DATA_STORAGE['widgets'].clear()

    def refresh(self, messages):
        self.clear()
        for msg in messages:
            msg_widget = MsgRowWidget(self.frame, msg)
            settings.DATA_STORAGE['widgets'][msg.ind] = msg_widget
            self.rows.append(msg_widget)
        for i, r in enumerate(self.rows):
            r.grid(row=i + 1, column=0, sticky='nw')
        self.align_columns()


class SimpleFieldWidget(tk.Frame):
    def __init__(self, master, name, default_value=""):
        tk.Frame.__init__(self, master)
        self.label = tk.Label(self, text=name)
        self.entry = tk.Entry(self)
        self.entry.insert(0, default_value)
        self.label.grid(row=0, column=0)
        self.entry.grid(row=1, column=0)

    def get(self):
        return self.entry.get()


class FilterWidget(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        self.checkboxes = dict()
        self.vars = dict()
        for i, type in enumerate(settings.LOCAL_MESSAGE_TYPES):
            self.vars[type] = tk.BooleanVar(self)
            self.vars[type].set(settings.CONFIG['show'][type])
            self.checkboxes[type] = tk.Checkbutton(self, text=type, variable=self.vars[type],
                                                   command=self.refresh_config)
            self.checkboxes[type].grid(row=i, column=0)

    def refresh_config(self, event=None):
        for type in self.vars:
            settings.CONFIG['show'][type] = self.vars[type].get()


class InfoWidget(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        pass


class MainWidget(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master)
        parent = self
        # parent = tk.Frame(master)
        # parent.grid(row=0, column=0, sticky=tk.E + tk.W)

        self.control_panel = tk.Frame(parent)
        self.control_panel.grid(row=0, column=0, sticky='w')
        self.result_id_widget = SimpleFieldWidget(self.control_panel, "ResultId",
                                                  "052b4855-627e-43c9-bc96-e8af0e289e0f")
        self.region_or_url_widget = SimpleFieldWidget(self.control_panel, "Region", 'eu')
        self.search_button = tk.Button(self.control_panel, text='Search', command=self.search)
        self.filter = FilterWidget(self.control_panel)
        self.refresh_button = tk.Button(self.control_panel, text='Refresh', command=self.refresh)

        self.result_id_widget.grid(row=0, column=0, sticky='ns')
        self.region_or_url_widget.grid(row=0, column=1, sticky='ns')
        self.search_button.grid(row=0, column=2, sticky='ns')
        self.filter.grid(row=0, column=3, sticky='ns')
        self.refresh_button.grid(row=0, column=4, sticky='ns')

        self.message_window = tk.Frame(parent, height=0)
        self.message_window.grid(row=1, column=0, columnspan=5, sticky='nswe')
        self.board = MsgBoardWidget(self.message_window)
        self.board.grid(row=0, column=0, sticky='nswe')
        # self.board.pack(side=tk.LEFT, expand=1, fill=tk.Y)
        default_text = 'Segment Reactions'
        self.show_react = tk.Label(self.message_window, text=default_text, relief="sunken",
                                   wraplength=400, anchor='n',
                                   height=0, width=60)
        self.show_react.grid(row=0, column=1, sticky='nsew')
        # self.show_react.pack(side=tk.RIGHT, expand=1, fill=tk.Y)
        global reaction_label
        global default_reaction_label_text
        reaction_label = self.show_react
        default_reaction_label_text = default_text

        self.message_window.rowconfigure(0, weight=1)
        self.message_window.columnconfigure(0, weight=1)

        # parent.rowconfigure(1, weight=1)
        parent.columnconfigure(0, weight=1)

    def search(self):
        self.clear()

        result_id = self.result_id_widget.get()
        if not result_id:
            mb.showwarning(title='Warning', message='ResultId field must not be empty')
            return
        region_or_url = self.region_or_url_widget.get()
        if not region_or_url:
            mb.showwarning(title='Warning', message='Region must not be empty')
            return

        # from utils import check_str_is_url
        #
        # c = check_str_is_url(region_or_url)
        # url = region_or_url if check_str_is_url(region_or_url) else prepare_url_from_region(region_or_url)

        url = prepare_url_from_region(region_or_url)
        response = get_devlogs(url, result_id)

        if response.status_code != 200:
            mb.showwarning(title='Warning', message=f'No results were found. Response code: {response.status_code}')
            return

        messages, message_indices, segments = preprocess_raw_logs(response.text)
        settings.DATA_STORAGE['msgs'] = messages
        settings.DATA_STORAGE['msg_indices'] = message_indices
        settings.DATA_STORAGE['segments'] = segments
        self.refresh()

    def refresh(self):
        self.board.clear()
        selected_msgs = form_message_selection()
        self.board.refresh(selected_msgs)

    def clear(self):
        self.board.clear()
        clear_data_storage()
