import json
import settings
from utils import form_message_selection, get_timestamp, Message, Segment, Reaction


incoming_msg_template = {
    "SpeechChannelMessage": {
        'type': ['Voice',
                 'PotentialPause',
                 'Pause']
    },
    "RawTextChannelMessage": {
        "textMsgId": ["FinalTextChannelMessage",
                      "ConfidentTextChannelMessage"]
    },
    # "RecognizedSpeechMessage": {},
}


outcoming_msg_template = {
    "RawTextChannelMessage"
}

reaction_template = {
    "NodeSwitchNotificationMessage": {
        "msgId": ["RecognizedSpeechMessage"]
    }
}


def check_row_is_non_ignored_message(row):
    # TODO make it with some kind of schema
    incoming = row['incoming']
    msg = row['msg']
    msgId = msg['msgId']
    if incoming:
        if msgId not in incoming_msg_template:
            return False
        if msgId == 'SpeechChannelMessage':
            if msg['type'] not in incoming_msg_template[msgId]['type']:
                return False
        if msgId == 'RawTextChannelMessage':
            if msg['textMsgId'] not in incoming_msg_template[msgId]['textMsgId']:
                return False
    else:
        if msgId not in outcoming_msg_template:
            return False
    return True


def check_row_is_non_ignores_reaction(row):
    # TODO make it with some kind of schema
    msg = row['msg']
    msgId = msg['msgId']
    if msgId not in reaction_template:
        return False
    if msg['msg']['msgId'] not in reaction_template[msgId]['msgId']:
        return False
    return True


def preprocess_raw_logs(raw_logs):
    messages = dict()
    message_indices = list()
    segments = dict()  # maps voiceSegmentId to Segment object

    logs_json = json.loads(raw_logs)
    incomplete_segment_ids = set()  # must be empty
    node_to_segment_id = dict()  # last visited node to segment id
    last_node = None
    last_segment_id = None
    for ind, row in enumerate(logs_json):
        if row['msg']['msgId'] == 'OpenedSessionChannelMessage' and settings.CONFIG['origin_time'] is None:
            settings.CONFIG['origin_time'] = get_timestamp(row['time'])
        if check_row_is_non_ignored_message(row):
            msg = Message(ind, row, last_segment_id)
            segment_id = msg.segment_id

            messages[ind] = msg
            message_indices.append(ind)


            if segment_id == '-':
                continue
            if segments.get(segment_id) is not None:
                segment = segments[segment_id]
            else:
                segment = Segment(segment_id)
                segments[segment_id] = segment
            segment.add_msg(msg)
            if msg.incoming is False:
                segment.reactions.append(msg)

            if segment.is_complete():
                incomplete_segment_ids.discard(segment_id)
            else:
                incomplete_segment_ids.add(segment_id)
        if check_row_is_non_ignores_reaction(row):
            reaction = Reaction(ind, row)
            segment_id = reaction.segment_id
            if segment_id is None:
                continue
            last_segment_id = segment_id
            # last_node = reaction.node_to
            # node_to_segment_id[last_node] = segment_id
            segment = segments[segment_id]
            segment.reactions.append(reaction)

    # assert len(incomplete_segment_ids) == 0  # voiceSegmentId == 0 ???
    return messages, message_indices, segments
