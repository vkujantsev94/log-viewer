import requests
import settings
import requests
from datetime import datetime
import tkinter as tk
import re

url_regexp = re.compile('https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)')


def check_str_is_url(s):
    return len(url_regexp.findall(s)) != 0

def form_message_selection():
    messages = settings.DATA_STORAGE['msgs']
    message_indices = settings.DATA_STORAGE['msg_indices']
    available_types = [t for t in settings.CONFIG['show'] if settings.CONFIG['show'][t] is True]
    selection = list()
    for ind in message_indices:
        msg = messages[ind]
        if msg.type in available_types:
            selection.append(msg)
    return selection


def get_max_word_length(s: str):
    return max(map(len, s.split(' ')))


def get_devlogs(url, result_id):
    return requests.get(f"{url}/api/v1/conversations/results/{result_id}/logsDev",
                       headers={f"Authorization": f"Bearer {settings.DASHA_APIKEY}"})


def prepare_url_from_region(region):
    return f"https://api.{region}.dasha.ai"


def clear_data_storage():
    data = settings.DATA_STORAGE
    for key in data:
        data[key].clear()
    settings.CONFIG = settings.create_config()


def get_timestamp(date_str):
    return int(datetime.strptime(date_str, settings.TIME_FORMAT).timestamp() * 1000)


class clr:
    R = '\033[91m'
    G = '\033[92m'
    B = '\033[94m'
    V = '\033[95m'
    C = '\033[96m'
    Y = '\033[93m'
    end = '\033[0m'


class Reaction:
    def __init__(self, ind, row):
        self.ind = ind
        self.absolute_time = get_timestamp(row['time']) - settings.CONFIG['origin_time']
        msg = row['msg']
        self.node_from = msg['from']
        self.node_to = msg['to']
        t = msg['transition']
        self.trans = t if t == 'PopStack' else 'Trans' + str(t)
        segment_id = msg['msg'].get('voiceSegmentId')
        self.segment_id = str(segment_id) if segment_id is not None else segment_id

    def __str__(self):
        return self.node_from + '\n--' + self.trans + '->\n' + self.node_to

    def format(self):
        return self.__str__()


class Message:
    def __init__(self, ind, row, last_segment_id=None):
        self.ind = ind
        self.absolute_time = get_timestamp(row['time']) - settings.CONFIG['origin_time']
        self.relative_time = None
        self.incoming = row['incoming']
        self.text = None
        self.type = None
        self.segment_id = last_segment_id
        if self.incoming:
            self.init_incoming_msg(row)
        else:
            self.init_outcoming_msg(row)

    def init_incoming_msg(self, row):
        msg = row['msg']
        msgId = msg['msgId']
        if msgId == 'SpeechChannelMessage':
            self.type = msg['type']
        elif msgId == 'RawTextChannelMessage':
            self.text = msg['text']
            self.type = msg['textMsgId'].replace('TextChannelMessage', '')
        self.segment_id = str(msg['voiceSegmentId'])

    def init_outcoming_msg(self, row):
        msg = row['msg']
        msgId = msg['msgId']
        self.text = msg['text']
        self.type = msgId.replace('TextChannelMessage', '')
        # TODO define corresponding segment_id
        if self.segment_id is None:
            self.segment_id = '-'
        else:
            # self.segment_id = "(" + str(self.segment_id) + ")"
            self.segment_id = str(self.segment_id)

    def __str__(self):
        """
        needed for debug mostly
        :return: string representation of message
        """
        # TODO str.ljust
        prefix = f"{clr.G}USER" if self.incoming else f"{clr.R}DASHA"
        rel_time = '--' if self.relative_time is None else self.relative_time
        res = "".join([
            f"{self.absolute_time}\t",
            f"{rel_time}\t"
            f"{self.segment_id}\t",
            prefix,
            f":\t({self.type})  "
        ])
        if self.text:
            res += self.text
        res += clr.end
        return res

    def format(self):
        res = f"USER: " if self.incoming else f"DASHA:"
        if self.text:
            res += self.text
        else:
            res += '---'
        return res

class Segment:
    #  нужен для:
    #   1. вычисление относительного времени
    #   2. хранение всех сообщений сегмента
    #   3. хранение реакций, связанных с сегментом
    def __init__(self, id):
        self.id = id
        self.voice = None
        self.potential_pauses = []
        self.final = None
        self.confident = None
        self.pause = None
        self.reactions = []
        self.segment_reaction = None

    def add_msg(self, msg):
        if msg.type == 'Voice':
            self.voice = msg
        elif msg.type == 'PotentialPause':
            self.potential_pauses.append(msg)
        elif msg.type == 'Final':
            self.final = msg
        elif msg.type == 'Confident':
            self.confident = msg
            self.__set_relative_times()
        elif msg.type == 'Pause':
            self.pause = msg
            # assert self.is_complete()    # voiceSegmentId == 0 ???
            self.__set_relative_times()

    def is_complete(self):
        return None not in [self.voice, self.final, self.pause]

    def segment_msg_generator(self):
        show = settings.CONFIG['show']
        if self.voice and show['Voice']:
            yield self.voice
        if show['PotentialPause']:
            for pp in self.potential_pauses:
                yield pp
        if self.final and show['Final']:
            yield self.final
        if self.pause and show['Pause']:
            yield self.pause
        if self.confident and show['Confident']:
            yield self.confident

    def get_segment_reaction(self):
        last_reaction_is_msg = False
        res = []
        for r in self.reactions:
            if isinstance(r, Message):
                if last_reaction_is_msg:
                    res[-1] = ' '.join([res[-1], r.text])
                else:
                    res.append(r.format())
                last_reaction_is_msg = True
            else:
                res.append(r.format())
                last_reaction_is_msg = False
        # return '\n\n'.join([x.format() for x in self.reactions])
        return '\n\n'.join(res)

    def __set_relative_times(self):
        if self.pause is None: return
        segment_origin_timestamp = self.pause.absolute_time
        msgs = [msg for msg in self.segment_msg_generator() if msg.relative_time is None]
        for msg in msgs:
            msg.relative_time = msg.absolute_time - segment_origin_timestamp

    def __str__(self):
        return f"""
        ------------
        |{self.voice}
        |{'->'.join(str(pp) for pp in self.potential_pauses)}
        |{self.final}
        |{self.confident}
        |{self.pause}
        ------------
        """