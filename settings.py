from collections import defaultdict
import tkinter as tk
import os

LOCAL_MESSAGE_TYPES = {
    "Voice",
    "PotentialPause",
    "Pause",
    "Final",
    "Confident",
    "Raw"
}


def create_config():
    c = dict()
    c['show'] = {}
    for t in LOCAL_MESSAGE_TYPES:
        c['show'][t] = True
    c['origin_time'] = None
    return c


def init():
    global CONFIG
    global DATA_STORAGE
    global DASHA_APIKEY
    global TIME_FORMAT
    global LOCAL_MESSAGE_TYPES

    CONFIG = create_config()
    DATA_STORAGE = defaultdict(dict)
    # TODO get it from env
    DASHA_APIKEY = os.environ.get("DASHA_APIKEY")  # console dasha.ai
    if DASHA_APIKEY is None:
        raise RuntimeError('DASHA_APIKEY is not found')
    TIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%fZ'


